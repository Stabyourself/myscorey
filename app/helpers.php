<?php
    function racetime($time) {
        return sprintf('%02d:%02d:%02d', floor($time / 3600), floor(($time / 60) % 60), $time % 60);
    }

    function placementLetters($placement) {
        $ends = array('th','st','nd','rd','th','th','th','th','th','th');
        if ((($placement % 100) >= 11) && (($placement%100) <= 13))
            return $placement. 'th';
        else
            return $placement. $ends[$placement % 10];
    }
