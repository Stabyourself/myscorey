<?php

namespace App\Models;

use App\Events\LeaderboardChanged;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    use HasFactory;

    protected $visible = [ // let's not give everyone everyone's passwords lmao
        'name',
        'score',
        'created_at'
    ];

    protected $fillable = ["name", "score", "password"];

    protected static function booted()
    {
        static::created(function ($entry) {
            LeaderboardChanged::dispatch($entry->leaderboard);
        });
        static::updated(function ($entry) {
            LeaderboardChanged::dispatch($entry->leaderboard);
        });
        static::deleted(function ($entry) {
            LeaderboardChanged::dispatch($entry->leaderboard);
        });
    }

    public function leaderboard() {
        return $this->belongsTo("App\Models\Leaderboard");
    }
}
