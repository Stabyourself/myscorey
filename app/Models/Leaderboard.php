<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Leaderboard extends Model
{
    use HasFactory;

    protected $dates = ["open_at", "closed_at"];
    protected $fillable = ["name", "slug", "instructions"];
    protected $appends = ["link"];

    static public function visible() {
        $now = Carbon::now()->addHours(1);
        return Leaderboard::where("open_at", "<", $now);
    }

    public function entries() {
        return $this->hasMany("App\Models\Entry")->orderBy("score", "desc");
    }

    public function getLinkAttribute() {
        return route("leaderboard.show", $this->slug);
    }

    public function getOpenAttribute() {
        $now = Carbon::now();
        return $now >= $this->open_at && $now < $this->closed_at;
    }

    public function getVisibleAttribute() {
        $now = Carbon::now();
        return $now >= $this->open_at->addHours(-1);
    }

    public function getEntryEnabledAttribute() {
        $now = Carbon::now();
        return $now >= $this->open_at->addHours(-1) && $now < $this->closed_at;
    }
}
