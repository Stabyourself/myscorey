<?php

namespace App\Http\Controllers;

use App\Models\Entry;
use App\Models\Leaderboard;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class EntryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Leaderboard $leaderboard)
    {
        $validated = $request->validate([
            "name" => "required|max:25",
            "password" => "required",
            "score" => "required",
        ]);

        // leaderboard's closed
        if (!$leaderboard->open) {
            if ($leaderboard->entry_enabled) {
                if (Carbon::now() <= $leaderboard->open_at) {
                    // allow entries for scores of 0
                    if (floatval($validated["score"]) != 0) {
                        abort(403, "You can only submit 0 before it starts.");
                    }
                }
            } else {
                abort(403, "This leaderboard is closed.");
            }
        }

        // only allow positive scores for now
        if (floatval($validated["score"]) < 0) {
            abort(403, "No negative scores allowed. Git gud.");
        }

        // check if the name exists already, and if so, attempt to override it
        $entry = $leaderboard->entries()->where("name", $validated["name"])->first();

        if ($entry) {
            if (!Hash::check($validated["password"], $entry->password)) {
                abort(403, "Wrong password for that name.");
            }

            // use the existing password. I dunno if hashing the same password many times is bad practice for whatever reason (like you could figure out the original with enough samples or something)
            $validated["password"] = $entry->password;

        } else {
            // hash the password like the good programmer we are
            $validated["password"] = Hash::make($validated["password"]);
        }

        $leaderboard->entries()->save(new Entry($validated));

        return "OK";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Entry  $entry
     * @return \Illuminate\Http\Response
     */
    public function show(Entry $entry)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Entry  $entry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $entry)
    {
        $validated = $request->validate([
            "name" => "required|max:25",
            "password" => "required",
            "score" => "required",
        ]);

        // check password
        if (!Hash::check($validated["password"], $entry->password)) {
            abort(403, "Wrong password for that name.");
        }

        unset($validated["password"]); // don't update the password

        $entry->update($validated);

        return "OK";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Entry  $entry
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entry $entry)
    {
        $entry->forceDelete();

        return "OK";
    }
}
