<?php

namespace App\Http\Controllers;

use App\Models\Leaderboard;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $leaderboards = Leaderboard::visible()->with("entries")->distinct("name")->get(["name", "slug", "id", "date"]);

        return view("home")->with(compact("leaderboards"));
    }
}
