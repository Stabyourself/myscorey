<?php

namespace App\Http\Controllers;

use App\Models\Entry;
use App\Models\Leaderboard;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LeaderboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            "name" => "required|max:30",
        ]);

        Leaderboard::create($validated);

        return "OK";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Entry  $entry
     * @return \Illuminate\Http\Response
     */
    public function show(Leaderboard $leaderboard)
    {
        if (!$leaderboard->visible) {
            abort(404);
        }

        $leaderboards = Leaderboard::visible()->get(["slug", "name"]);

        return view("leaderboard")->with(compact("leaderboard", "leaderboards"));
    }

    public function showAscii(Leaderboard $leaderboard)
    {
        if (!$leaderboard->visible) {
            abort(404);
        }

        return view("leaderboard-ascii")->with(compact("leaderboard"));
    }

    public function showChart(Leaderboard $leaderboard)
    {
        if (!$leaderboard->visible) {
            abort(404);
        }

        return view("leaderboard-chart")->with(compact("leaderboard"));
    }

    public function showInstructions(Leaderboard $leaderboard)
    {
        if (!$leaderboard->visible) {
            abort(404);
        }

        return view("leaderboard-instructions")->with(compact("leaderboard"));
    }

    public function showHidden(Leaderboard $leaderboard)
    {
        $leaderboards = Leaderboard::get(["slug", "name"]);

        return view("leaderboard")->with(compact("leaderboard", "leaderboards"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Entry  $entry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Leaderboard $Leaderboard)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Entry  $entry
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entry $entry)
    {
        $entry->forceDelete();

        return "OK";
    }
}
