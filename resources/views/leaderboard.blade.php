@extends('layouts.app')

@section('content')
    <v-app>
        <leaderboard
            :leaderboard="{{ json_encode($leaderboard->only(["slug", "name", "instructions", "open_at", "closed_at", "entry_enabled"])) }}"
            :leaderboards="{{ json_encode($leaderboards) }}"
            :initial-entries="{{ json_encode($leaderboard->entries) }}"
        ></leaderboard>
    </v-app>
@endsection
