@extends('layouts.app')

@section('content')
    <v-app>
        <home
            :leaderboards="{{ json_encode($leaderboards) }}"
        ></home>
    </v-app>
@endsection
