@extends('layouts.app')

@section('content')
    <v-app style="padding: 20px">
        <leaderboard-instructions
            markdown="{{ $leaderboard->instructions }}"
        ></leaderboard-instructions>
    </v-app>
@endsection
