@extends('layouts.app')

@push("style")
    <link href="{{ asset(mix("css/ascii.css")) }}" rel="stylesheet">
@endpush

@section('content')
    <v-app>
        <leaderboard-ascii
            leaderboard-slug="{{ $leaderboard->slug }}"
            :initial-entries="{{ json_encode($leaderboard->entries) }}"
        ></leaderboard-ascii>
    </v-app>
@endsection
