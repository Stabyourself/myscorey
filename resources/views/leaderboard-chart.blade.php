@extends('layouts.app')

@push("style")
    <link href="{{ asset(mix("css/ascii.css")) }}" rel="stylesheet">
@endpush

@section('content')
    <v-app>
        <leaderboard-chart
            :leaderboard="{{ json_encode($leaderboard->only(["slug", "open_at", "closed_at"])) }}"
            :initial-entries="{{ json_encode($leaderboard->entries) }}"
        ></leaderboard-chart>
    </v-app>
@endsection
