var md = require('markdown-it')();

function renderMarkdown(s) {
    var html = md.render(s)

    // shitty hack time
    html = html.replaceAll("<table>", '<div class="v-data-table theme--dark"><div class="v-data-table__wrapper"><table>')
    html = html.replaceAll("</table>", '</table></div></div>')

    return html
}

export {
    renderMarkdown
}
