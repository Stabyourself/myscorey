// src/plugins/vuetify.js

import Vue from 'vue'
import Vuetify, {VApp} from 'vuetify/lib'

Vue.use(Vuetify, {
    components: {
        VApp,
    }
})

const opts = {
    theme: { dark: true },
}

export default new Vuetify(opts)