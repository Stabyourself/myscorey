<?php

namespace Database\Seeders;

use App\Models\Entry;
use App\Models\Leaderboard;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    public function run()
    {
        Leaderboard::create([
            "name" => "Tony Hawk's Pro Skater 4",
            "slug" => "THPS4",
            "date" => date("Y-m-d"),
            "open_at" => "2021-04-23T18:00:00",
            "closed_at" => "2021-04-23T20:00:00",
            "instructions" => "
#### Controls
Button       | Action
------------ | ------------
D-Pad        | Move (Left/Right)
Cross        | Hold to roll a block

#### Ready Info
- Choose Normal Game
- Make sure you're on Normal Difficulty the choose Game Start (you can change bgm if you want I guess)

#### Game & Scoring Info
- You move left and right and can walk up walls
- Your goal is to match up the colors on the cube so that a diamond is made
    - A vertical match spawns 2 cubes under that row
    - A horizontal match spawns 2 cubes under each row
- You move blocks by rolling one at a time under your feet by holding the x button
    - A match won't happen until you let go of the roll button
    - Certain blocks have arrows on them which indicates they can only be rolled in one direction
    - Blocks can be rolled up on a stack of one block height
    - If you roll a block up, then back down, the next time you roll it up it will be rotated 90 degrees, you can manipulate blocks in this way but it can be slow
- The tower is steadily crumbling from below, and if you don't match cubes fast enough you will lose
    - The stack on the left indicates how many rows you need to build up before moving to the next level
    - Any red indicator means that that row has already crumbled
    - The clock at the bottom right shows when the next row will crumble
- There is a combo meter for making several matches quickly, this will improve your score and grow bonus cubes to let you finish the level quicker
- There are 8 levels and letting the tower crumble will be a game over.
- There is a step indicator in the bottom right, if you complete the level under that number of steps you'll receive a bonus at the end of the level
- If you have a completely flat tower such that you can't roll anything you get a ring bonus and you get some bonus cubes, but this situation is difficult to manufacture
"
        ]);
    }
}
