<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'home'])->name('home');

Route::get('/{leaderboard:slug}', [App\Http\Controllers\LeaderboardController::class, 'show'])->name("leaderboard.show");
Route::get('/{leaderboard:slug}/ascii', [App\Http\Controllers\LeaderboardController::class, 'showAscii']);
Route::get('/{leaderboard:slug}/chart', [App\Http\Controllers\LeaderboardController::class, 'showChart']);
Route::get('/{leaderboard:slug}/instructions', [App\Http\Controllers\LeaderboardController::class, 'showInstructions']);

Route::get('/{leaderboard:slug}/supersecret', [App\Http\Controllers\LeaderboardController::class, 'showHidden']);

Route::post('/{leaderboard:slug}/entry', [App\Http\Controllers\EntryController::class, 'store']);